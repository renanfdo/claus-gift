﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Velocidade : MonoBehaviour {

	private Vector2 velocimetro = new Vector2 (-4f, 0f);
	public static float velocidade = 1;

	// Use this for initialization
	void Start () {
		if (gameObject.GetComponent<Rigidbody2D>().tag.IndexOf("casa-frente") >= 0)
			velocimetro = new Vector2 ((float)(-7 * velocidade), 0f);
		else
			velocimetro = new Vector2 ((float)(-4 * velocidade), 0f);

		gameObject.GetComponent<Rigidbody2D>().velocity = velocimetro;
	}
	
	void Update () {
		if (gameObject.GetComponent<Rigidbody2D>().tag.IndexOf("casa-frente") >= 0)
			velocimetro = new Vector2 ((float)(-7 * velocidade), 0f);
		else
			velocimetro = new Vector2 ((float)(-4 * velocidade), 0f);
		
		gameObject.GetComponent<Rigidbody2D>().velocity = velocimetro;
	}
}
