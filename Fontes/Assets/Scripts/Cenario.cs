﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Cenario : MonoBehaviour {

	public GameObject obj_nuvem;
	public GameObject obj_casa_fundo;
	public GameObject obj_casa_frente;

	public AudioSource as_back;
	public Text t_mute;

	private ArrayList arr_nuvem = new ArrayList ();
	private ArrayList arr_casa_fundo = new ArrayList ();
	private ArrayList arr_casa_frente = new ArrayList ();

	public static int i_count_nuvem = int.MaxValue-1;
	public static int i_count_casa_fundo = int.MaxValue-1;
	public static int i_count_casa_frente = int.MaxValue-1;

	public static float f_freq_nuvem = 7f;
	public static float f_freq_casa_fundo = 3.6f;
	public static float f_freq_casa_frente = 2.8f;
	
	void Start () {
		i_count_nuvem = int.MaxValue-1;
     	i_count_casa_fundo = int.MaxValue-1;
		i_count_casa_frente = int.MaxValue-1;

		f_freq_nuvem = 7f;
		f_freq_casa_fundo = 3.6f;
		f_freq_casa_frente = 2.8f;

		Velocidade.velocidade = 1;

		as_back.mute = PlayerPrefs.GetInt ("Mute") == 1;
		t_mute.text = "Mute: " + (PlayerPrefs.GetInt ("Mute") == 1 ? "ON" : "OFF");
	}

	void FixedUpdate ()
	{
		i_count_nuvem++;
		i_count_casa_fundo++;
		i_count_casa_frente++;

		try {
			GameObject nuvem = (GameObject)arr_nuvem [1];

			if (nuvem.GetComponent<Rigidbody2D>().position.x < 0) {
				i_count_nuvem = 0;
				CriarNuvem ();
			}
		} catch(Exception e){
			if (f_freq_nuvem/0.02 <= i_count_nuvem) {
				i_count_nuvem = 0;
				CriarNuvem ();
			}
		}

		try {
			GameObject casa = (GameObject)arr_casa_fundo [2];

			if (casa.GetComponent<Rigidbody2D>().position.x < 2) {
				i_count_casa_fundo = 0;
				CriarVilaFundo ();
			}
		} catch(Exception e){
			if (f_freq_casa_fundo/0.02 <= i_count_casa_fundo) {
				i_count_casa_fundo = 0;
				CriarVilaFundo ();
			}
		}

		try {
			GameObject casa = (GameObject)arr_casa_frente [2];

			if (casa.GetComponent<Rigidbody2D>().position.x < 5) {
				i_count_casa_frente = 0;
				CriarVilaFrente ();
			}
		} catch(Exception e){
			if (f_freq_casa_frente/0.02 <= i_count_casa_frente) {
				i_count_casa_frente = 0;
				CriarVilaFrente ();
			}
		}
	}

	void CriarNuvem(){
		arr_nuvem.Add(Instantiate (obj_nuvem));

		if (arr_nuvem.Count >= 3) {
			Destroy ( (GameObject) arr_nuvem[0] );
			arr_nuvem.RemoveAt (0);
		}
	}

	void CriarVilaFundo(){
		arr_casa_fundo.Add(Instantiate (obj_casa_fundo));

		if (arr_casa_fundo.Count >= 4) {
			Destroy ( (GameObject) arr_casa_fundo[0] );
			arr_casa_fundo.RemoveAt (0);
		}
	}

	void CriarVilaFrente(){
		arr_casa_frente.Add(Instantiate (obj_casa_frente));

		if (arr_casa_frente.Count >= 4) {
			Destroy ( (GameObject) arr_casa_frente[0] );
			arr_casa_frente.RemoveAt (0);
		}
	}

	public void onMute() {
		PlayerPrefs.SetInt ("Mute", (PlayerPrefs.GetInt ("Mute") == 1 ? 0 : 1));

		t_mute.text = "Mute: " + (PlayerPrefs.GetInt ("Mute") == 1 ? "ON" : "OFF");
		as_back.mute = PlayerPrefs.GetInt ("Mute") == 1;
	}
}
