﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartGamer : MonoBehaviour {

	private ArrayList arr_gameobject = new ArrayList ();
	public GameObject obj_cilindro;
	public GameObject obj_circulo;
	public GameObject obj_coracao;
	public GameObject obj_quadrado;
	public AudioSource as_back;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("ChuvaPresentes", 0.5f, 0.5f);

		as_back.mute = PlayerPrefs.GetInt ("Mute") == 1;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void ChuvaPresentes(){
		int p = UnityEngine.Random.Range (1, 5);
		switch (p) {
		case 1:
			this.obj_cilindro.transform.SetPositionAndRotation(new Vector3(UnityEngine.Random.Range (-10, 10),22),new Quaternion());
			arr_gameobject.Add (Instantiate (this.obj_cilindro));
			break;
		case 2:
			this.obj_circulo.transform.SetPositionAndRotation(new Vector3(UnityEngine.Random.Range (-10, 10),22),new Quaternion());
			arr_gameobject.Add (Instantiate (this.obj_circulo));
			break;
		case 3:
			this.obj_coracao.transform.SetPositionAndRotation(new Vector3(UnityEngine.Random.Range (-10, 10),22),new Quaternion());
			arr_gameobject.Add (Instantiate (this.obj_coracao));
			break;
		case 4:
			this.obj_quadrado.transform.SetPositionAndRotation(new Vector3(UnityEngine.Random.Range (-10, 10),22),new Quaternion());
			arr_gameobject.Add (Instantiate (this.obj_quadrado));
			break;
		}

		if (arr_gameobject.Count > 10) {
			Destroy ( (GameObject) arr_gameobject[0] );
			arr_gameobject.RemoveAt (0);
		}
	}

	public void Play() {
		Application.LoadLevel ("Claus Gift");
	}
}
