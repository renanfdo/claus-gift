﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public static int i_life;
	private static bool b_pause;
	public GameObject go_pause;

	// Use this for initialization
	void Start () {
		i_life = 3;
		b_pause = false;
	}
	
	// Update is called once per frame
	void Update () {
		GameObject l = GameObject.Find ("life" + (i_life + 1));
		Destroy (l);

		if (i_life == 0) {
			if (Score.scoreValue > PlayerPrefs.GetInt ("Highscore"))
				PlayerPrefs.SetInt ("Highscore", Score.scoreValue);
			Application.LoadLevel ("Play Game");
		}
	}

	public void cliqueBotao(GameObject obj) {
		Instantiate (obj);
	}

	public void onPause() {
		b_pause = !b_pause;

		if (b_pause)
			Time.timeScale = 0;
		else
			Time.timeScale = 1;

		go_pause.SetActive (b_pause);
	}
}
