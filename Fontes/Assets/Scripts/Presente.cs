﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Presente : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D other){
		if ((other.gameObject.tag == "casa-frente-cilindro" && this.gameObject.tag == "presente-cilindro") ||
			(other.gameObject.tag == "casa-frente-circulo" && this.gameObject.tag == "presente-circulo") ||
			(other.gameObject.tag == "casa-frente-coracao" && this.gameObject.tag == "presente-coracao") ||
			(other.gameObject.tag == "casa-frente-quadrado" && this.gameObject.tag == "presente-quadrado")){
			Score.scoreValue++;

			if (Score.scoreValue % 3 == 0) {
				Velocidade.velocidade += 0.1f;
			}

			GameObject casa = other.gameObject;
			casa.GetComponent<Rigidbody2D>().tag = "casa-frente";

			Destroy (this.gameObject);
		} else if ((other.gameObject.tag == "casa-frente-cilindro" && this.gameObject.tag != "presente-cilindro") ||
			(other.gameObject.tag == "casa-frente-circulo" && this.gameObject.tag != "presente-circulo") ||
			(other.gameObject.tag == "casa-frente-coracao" && this.gameObject.tag != "presente-coracao") ||
			(other.gameObject.tag == "casa-frente-quadrado" && this.gameObject.tag != "presente-quadrado") ||
			(other.gameObject.tag == "casa-frente") || (other.gameObject.tag == "Untagged")) {
			Player.i_life--;
			Destroy (this.gameObject);
		}
	}
}
