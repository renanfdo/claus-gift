﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Casas : MonoBehaviour {

	public string presente;
	private GameObject obj_indicador;
	private GameObject obj_presente;

	public GameObject presente_cilindro;
	public GameObject presente_circulo;
	public GameObject presente_coracao;
	public GameObject presente_quadrado;

	public GameObject indicador_cilindro;
	public GameObject indicador_circulo;
	public GameObject indicador_coracao;
	public GameObject indicador_quadrado;

	// Use this for initialization
	void Start () {
		int p = UnityEngine.Random.Range (1, 10);
		if (p % 2 == 0) {
			switch (p) {
			case 2:
				this.presente = "cilindro";
				obj_presente = Instantiate (presente_cilindro);
				obj_indicador = Instantiate (indicador_cilindro);
				break;
			case 4:
				this.presente = "circulo";
				obj_presente = Instantiate (presente_circulo);
				obj_indicador = Instantiate (indicador_circulo);
				break;
			case 6:
				this.presente = "coracao";
				obj_presente = Instantiate (presente_coracao);
				obj_indicador = Instantiate (indicador_coracao);
				break;
			case 8:
				this.presente = "quadrado";
				obj_presente = Instantiate (presente_quadrado);
				obj_indicador = Instantiate (indicador_quadrado);
				break;
			}
			gameObject.GetComponent<Rigidbody2D>().tag = "casa-frente-" +  this.presente;
		}
	}

	void FixedUpdate() {
		if (gameObject.tag != "casa-frente")
			if (obj_presente.GetComponent<Rigidbody2D> ().position.x < 12) {
				Destroy (this.obj_indicador);
			}
	}

	void Update () {
		if (gameObject.tag == "casa-frente") {
			Destroy (this.obj_presente);
		}
	}

	void OnDestroy() {
		Destroy (obj_presente);
	}
}
